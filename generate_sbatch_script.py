import argparse

#Input parameters needed : input and output directory (. as default)
def arguments():
    """Parsing the input arguments"""

    parser = argparse.ArgumentParser(
        description="Helps to generate a template sbatch script to run AlphaPullDown container on Euler",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-i",
        "--input_folder",
        help="Path to the input folder where fasta and baits files are contained",
        type=str,
        required=True
    )

    parser.add_argument(
        "-o",
        "--output_folder",
        help="Path to the output folder where you wish to store your the results of your run",
        type=str,
        required=True
    )
    return parser.parse_args()

def main():
    args = arguments()

    f=open(args.input_folder+'/run_alphapulldown_job.sbatch','w')

    f.write(f'#!/usr/bin/bash\n')
    f.write(f'#SBATCH -n 8              #8 cores required as default\n')
    f.write(f'#SBATCH --nodes 1         #this ensures that all cores are allocated on one node\n')
    f.write(f'#SBATCH --mem-per-cpu 10g #RAM requirements. Default value : 8*10GB=80GB\n')
    f.write(f'#SBATCH --time 04:00:00   #Runtime for the job\n')
    f.write(f'#SBATCH -J alphapulldown\n')
    f.write(f'#SBATCH -e {args.output_folder}/APD_%j_err.txt\n')
    f.write(f'#SBATCH -o {args.output_folder}/APD_%j_out.txt\n')
    f.write(f'\n')
    f.write(f'# IF REQUIRED, ADD THE GPU RESOURCES, e.g. #SBATCH -G 1  #SBATCH --gres=gpumem:XXg\n')
    f.write(f'# (ONE #SBATCH PRAGMA PER LINE, TO ADD TO OTHER #SBATCH PRAGMAS BEFORE THIS COMMENT)\n')
    f.write(f'\n')
    f.write(f'ALPHAPULLDOWN_IMAGE="/cluster/apps/nss/alphafold/containers/AlphaPulldown/alphapulldown.sif"\n')
    f.write(f'BINDS="{args.input_folder}:/mnt/inputs:ro,/mnt/params/:/cluster/project/alphafold/params/:ro,/cluster/project/alphafold/uniref90:/mnt/uniref90:ro,/cluster/project/alphafold/mgnify:/mnt/mgnify:ro,/cluster/project/alphafold/pdb_mmcif:/mnt/pdb_mmcif:ro,/cluster/project/alphafold/pdb70:/mnt/pdb70:ro,/cluster/project/alphafold/uniref30:/mnt/uniref30:ro,/cluster/project/alphafold/pdb_seqres:/mnt/pdb_seqres:ro,/cluster/project/alphafold/bfd:/mnt/bfd:ro,/cluster/project/alphafold/uniprot:/mnt/uniprot:ro,{args.output_folder}:/mnt/outputs:rw"\n')
    f.write(f'\n')
    f.write(f'# ---------------------------------------------------------------------------\n')
    f.write(f'# PLEASE CHOOSE THE COMMAND TO RUN AND COMPLETE THE NECESSARY INFORMATION\n')
    f.write(f'# ERASE THE UNNECESSARY COMMAND\n')
    f.write(f'# EVERYTHING IN BETWEEN <...> HAS TO BE REPLACED WITH APPROPRIATE VALUES/DATA\n')
    f.write(f'# PLEASE DO NOT FORGET TO REMOVE THE \'<\' AND \'>\' CHARACTERS BEFORE THE RUN\n')
    f.write(f'# THE \'\\\' AT THE END OF LINES ARE REQUIRED\n')
    f.write(f'# ---------------------------------------------------------------------------\n')
    f.write(f'\n')
    f.write(f'COMMAND="create_individual_features.py \\\n')
    f.write(f'--fasta_paths=/mnt/inputs/<baits.fasta>,/mnt/inputs/<sequences.fasta> \\\n')
    f.write(f'--data_dir=/mnt/ \\\n')
    f.write(f'--save_msa_files=<True | False> \\\n')
    f.write(f'--output_dir=/mnt/outputs \\\n')
    f.write(f'--use_precomputed_msas=<True | False> \\\n')
    f.write(f'--max_template_date=<any date you want, format like: 2050-01-01> \\\n')
    f.write(f'--skip_existing=<True | False> \\\n')
    f.write(f'--seq_index=<any number you want or skip the flag to run all one after another>"\n')
    f.write(f'\n')
    f.write(f'# OR\n')
    f.write(f'\n')
    f.write(f'COMMAND="run_multimer_jobs.py \\\n')
    f.write(f'--mode=<pulldown | custom | homo-oligomer> \\\n')
    f.write(f'--num_cycle=<number of cycle per each model prediction> \\\n')
    f.write(f'--num_predictions_per_model=<number of predictions computer per each model> \\\n')
    f.write(f'--output_path=/mnt/outputs \\\n')
    f.write(f'--data_dir=/mnt/ \\\n')
    f.write(f'--protein_lists=/mnt/inputs/<baits.txt>,/mnt/inputs/<candidates.txt> # This option is specific for pulldown and custom mode \\\n')
    f.write(f'--oligomer_state_file=/mnt/inputs/<oligomer.txt> # This option is specific for homo-oligomer mode \\\n')
    f.write(f'--monomer_objects_dir=</path/to/monomer_objects_directory> \\\n')
    f.write(f'--job_index=<any number you want>"\n')
    f.write(f'\n')
    f.write(f'# ---------------------------------------------------------------------------\n')
    f.write(f'# PLEASE DO NOT ERASE THE FOLLOWING LINE,\n')
    f.write(f'# AS IT IS THE FULL COMMAND TO RUN THE CONTAINER:\n')
    f.write(f'# ---------------------------------------------------------------------------\n')
    f.write(f'singularity run --nv --bind $BINDS $ALPHAPULLDOWN_IMAGE $COMMAND\n')

    f.close()

    print(f"I have generated a template sbatch script stored in {args.input_folder}"+"/run_alphapulldown_job.sbatch")
    print(f"Please follow the instructions inside the sbatch script to complete the commands and start the job")

if __name__ == '__main__':
    main()

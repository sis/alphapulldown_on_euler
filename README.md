
In this document, we will describe how to run the AlphaPulldown container on Euler. 
The accompanying script generates a template sbatch script that can be submitted as a job on the cluster.
At the moment, this is a tentative script that will be improved on progressively. 
All suggestions are welcome and should be addressed to nmarounina@ethz.ch.

The container build instructions and the corresponding documents has been provided to us by the IT team of Human Technopole of Milan. We modified the code and the documentation for the use on Euler, but hereby we wish to aknowledge and thank the IT team for their kind help.

 
# Introduction
AlphaPulldown is a Python package that streamlines protein-protein interaction screens and high-throughput modelling of higher-order oligomers using the AlphaFold-Multimer algorithm. In particular:

- provides a convenient command line interface to screen a bait protein against many candidates, calculate all-versus-all pairwise comparisons, test alternative homo-oligomeric states, and model various parts of a larger complex
- allows modeling fragments of proteins without recalculation of MSAs and keeping the original full-length residue numbering in the models
- summarizes the results in a CSV table with AlphaFold scores, pDockQ and mpDockQ, PI-score, and various physical parameters of the interface
- provides a Jupyter notebook for an interactive analysis of PAE plots and models

AlphaPulldown splits the pipeline into two steps:

- MSA and template feature generation: computed in the CPU.
- Protein structure modeling: computed in the GPU. In particular, this step can be run in multiple modes:
    - Pulldown: this mode is inspired by pull-down assays. One can specify one or more proteins as "bait" and another list of proteins as "candidates". The programme will use Alphafold Multimer to predict interactions between baits and candidates.
    - All vs All: this mode is similar to pulldown, with models computed between each pair of sequences in two sets. 
    - Custom: this mode is useful to model the interaction between two large sequences. If the region of interaction of the two sequences is known, it's possible to model specifically the region of interaction. 
    - Homo-oligomer: this mode is used to model the oligomer state of a protein, allowing to create predictions for monomeric, homodimeric, homotrimeric, etc. complexes.

In the AlphaPulldown repository you can find a well documented tutorial on pulldown mode and on custom and homo-oligomer mode. In this article we will show an example way of running prediction in pulldown mode, since the other modes are fairly similar and only need an additional configuration file that is illustrated in the previous links.

# Run AlphaPulldown on Euler
You can run AlphaPulldown on Euler using its Singularity container.

AlphaPulldown requires some databases and parameters to work, that are stored in /cluster/project/alphafold. The provided scripts will help to craft the singularity command to make the appropriate bindings so those databases are properly located in the container.

 
## Prepare input data
After logging in the HPC, you should move to the directory where you want to store you input and output files for AF2Complex, this could be for example the scratch:

```
mkdir /cluster/scratch/<username>/alphapulldown
cd /cluster/scratch/<username>/alphapulldown
```

There, you can prepare the input files required by AlphaPulldown :

- One (or more, depending on the mode) .fasta file, that can either contain a single or multiple sequences.
- A .txt file, that contains the information on how each predicted model should be composed. The content depends on the mode AlphaPulldown is run. For pulldown mode, a candidates.txt file is needed, which contains on each line the ID of the protein sequence (as in the fasta files) to be modeled together with the bait.

Also, AlphaPulldown writes output files to a user-defined output directory: for convenience, you could create the output directory in this same location.

```shell
mkdir /cluster/scratch/<username>/alphapulldown/output_data
```
 
## Usage of this script

First, load a python 3 module. Any recent python 3 version would do, e.g.:
```shell
module load gcc/8.2.0 python/3.11.2
```

Then run the script provided in this repo :

```shell
$python3 generate_sbatch_script.py -i <input directory containing fasta/bait files> -o <output directory for results>
$ I have generated a template sbatch script stored in ./run_alphapulldown_job.sbatch
$ Please follow the instructions inside the sbatch script to complete the commands and start the job
```

This would generate a template script that would pre-fill the commands to run the alphapulldown container. 
You would then need to go and modify the file to complete the alphapulldown command. 
Since the feature generation uses only CPUs while the other steps use GPUs to predict and minimise the model, you should make sure to allocate proper resources in the sbatch script, where suggestions are provided.
Once everything looks good, submit the job on Euler with :

```shell
sbatch run_alphapulldown_job.sbatch
```



# AlphaPulldown scripts
AlphaPulldown contains different scripts to perform different tasks:

- create_individual_features.py: this script is used for the first step of feature generation. 
It reads as input the protein sequence and uses the sequence databases of AlphaFold to output the features (in python pickle format, .pkl) that are then used to predict the protein structure. 
The script has mostly the same flags as AlphaFold, with a couple of additions that allow to launch jobs in parallel for multiple sequences. 
Generally, the script would include these flags:
 
```
create_individual_features.py
--fasta_paths=</path/to/baits.fasta>,</path/to/sequences.fasta>
--data_dir=<path to alphafold databases>
--save_msa_files=<True | False>
--output_dir=</path/to/output_dir> 
--use_precomputed_msas=<True | False> 
--max_template_date=<any date you want, format like: 2050-01-01>
--skip_existing=<True | False>
--seq_index=<any number you want or skip the flag to run all one after another>
```
 
- run_multimer_jobs.py: this script is used to predict a multimer protein structure by using the features computed in the first step. 
Generally, these argument are used to run this step of AlphaPulldown :

```
run_multimer_jobs.py
--mode=<pulldown | custom | homo-oligomer> \
--num_cycle=<number of cycle per each model prediction> \
--num_predictions_per_model=<number of predictions computer per each model>
--output_path=</path/to/output_dir>
--data_dir=<path to alphafold databases> 
--protein_lists=</path/to/baits.txt>,</path/to/candidates.txt> # This option is specific for pulldown and custom mode
--oligomer_state_file=</path/to/oligomer.txt> # This option is specific for homo-oligomer mode
--monomer_objects_dir=</path/to/monomer_objects_directory>
--job_index=<any number you want>
```
 
For both commands, you can run them with the --help or --helpfull flags to see the full list of options available.

All these scripts are in the PATH of the container: by running the container with the ```singularity run command```, you can access them directly. For more details on what each script does, you can refer to the first and second example in the AlphaPulldown repository ([example_1](https://github.com/KosinskiLab/AlphaPulldown/blob/main/manuals/example_1.md), [example_2](https://github.com/KosinskiLab/AlphaPulldown/blob/main/manuals/example_2.md)).
